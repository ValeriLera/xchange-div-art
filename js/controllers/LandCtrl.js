(function(){

	angular.module("app").controller("LandCtrl", ['$scope','$window', '$location', 'exchange', 'logger', 'request', 'resize', LandCtrl]);

	function LandCtrl($scope, $window, $location, exchange, logger, request, resize)
	{
		/*
		** The text for the error logger on the frontend
		** Текст ошибок с фронтенда
		*/
		$scope.text = {
			errors : [
				'Используйте разные платежные системы',
				'Неправильно заполнена форма',
				'Максимальное значение',
				'Минимальное значение'
			]
		}

		/*
		** DATA FOR get_quotes default
		** Значения по умолчанию при иницилизации
		*/

		$scope.exchange = {
			incoming_currency : 'USD',
			incoming_payed_amount : 0,
			incoming_payment_system : 'CreditCard',

			outgoing_currency : 'BTC',
			outgoing_payment_system : 'Bitcoin',
			outgoing_received_amount :0
		};

		/*
		** REQUEST get payment_systems
		** Получение данных при инициализации
		*/

		$scope.payment_systems = [];

		request.send('/payment_systems', {}, function(response) {
			$scope.getDefaultedIndex(response);
			//exchange.init(response);


			//$scope.defaultedCurrency = exchange.getDefaultedIndex(response);
			//$scope.getDefaultIndex($scope.defaultedCurrency);

			$scope.payment_systems = response;
			$scope.getDefaultedCurrencyInitLimit();
		}, 'get', true);

		/*
		** Direction of account in fields
		** Направление счета в полях
		*/

		$scope.direction = 'left';

		/*
		** We get the default values ​​of the currency, depending on whether
		** which system will arrive with the default value default_in / default_out == true
		** Получаем дефолтные значения валюты в зависимости от того
		** какая система прилетит дефолтной со значениями default_in/default_out == true
		*/

		/*$scope.getDefaultIndex = function (arr) {
			$scope.defaultedCurrencyIn = $scope.defaultedCurrency[0];
			$scope.defaultedCurrencyOut = $scope.defaultedCurrency[1];
		}*/

		$scope.getDefaultedIndex = function (arr) {
			arr.filter(function(item, i) {
				item.default_in ? $scope.defaultedCurrencyIn = i : false;
				item.default_out ? $scope.defaultedCurrencyOut = i : false;
			});
		}

		/*
		** When changing the payment system, we change currencies to currencies
		** belonging to this payment system
		** При изменении платежнной системы меняем  валюты на валюты
		** принадлежащие этой платежной системе ;
		** еслм валюта есть -- она чекнутая, если нет -- чекается первая в списке
		*/

		$scope.getIndexDefaultedCurrency = function (index, value) {
			$scope[index] = value;
			var curr = '';

			if (index == 'defaultedCurrencyIn') {
					if ( ! $scope.payment_systems[value].currencies.filter(function(i){ return i.code == $scope.exchange.incoming_currency; }).length) {
					curr = $scope.payment_systems[value].currencies[0].code;
					$scope.exchange.incoming_currency = curr;
				}
			} else {
					if ( ! $scope.payment_systems[value].currencies.filter(function(i){ return i.code == $scope.exchange.outgoing_currency; }).length) {
					curr = $scope.payment_systems[value].currencies[0].code;
					$scope.exchange.outgoing_currency = curr;
				}
			}
		}

		/*
		** GET LIMITS
		** Получение лимитов и установка дефолтных значений
		*/

		$scope.names = ['left', 'right'];

		$scope.getLimits = function(key, currency, direction) {
			key = " " + key || false;
			key = key * 1;
			currency = currency || false;
			if (currency) {
				if (direction == 'left') {
					$scope.minIn = currency.limits.in.min * 1;
					$scope.maxIn = currency.limits.in.max * 1 == 0 ? 0 : currency.limits.in.max * 1;
				} else if (direction == 'right') {
					$scope.minOut = currency.limits.out.min * 1;
					$scope.maxOut = currency.limits.out.max * 1 == 0 ? 0 : currency.limits.out.max * 1;
				}
			} else {

				if (direction == 'left') {
					var currIn = $scope.payment_systems[key].currencies.filter(function(i){ return i.code == $scope.exchange.incoming_currency; })
					$scope.minIn = currIn[0].limits.in.min * 1;
					$scope.maxIn = currIn[0].limits.in.max * 1 == 0 ? 0 : currIn[0].limits.in.max * 1;
				} else if (direction == 'right') {
					var currOut =  $scope.payment_systems[key].currencies.filter(function(i){ return i.code == $scope.exchange.outgoing_currency; })
					$scope.minOut = currOut[0].limits.out.min * 1;
					$scope.maxOut = currOut[0].limits.out.max * 1 == 0 ? 0 : currOut[0].limits.out.max * 1;
				}
			}
		}

		/*
		** Getting Limits and Installing in the Default Currency When Initializing
		** Получение лимитов и установка в дефолтной валюте при инициализации
		*/

		$scope.getDefaultedCurrencyInitLimit = function() {
			$scope.payment_systems.filter(function(item, i){
				if (item.default_in) {
					item.currencies.map(function(name) {
						return name.code == 'USD' ?  $scope.minIn =  name.limits.in.min * 1 : false;
					});

					item.currencies.map(function(name) {
						return name.code == 'USD' ?  $scope.maxIn =  (name.limits.in.max == 0 ? 0 : name.limits.in.max * 1) : false;
					});
				}
				if (item.default_out) {
					item.currencies.map(function(name) {
						return name.code == 'BTC' ?  $scope.minOut =  name.limits.out.min * 1 : false;
					});

					item.currencies.map(function(name) {
						return name.code == 'BTC' ?  $scope.maxOut =  (name.limits.out.max == 0 ? 0 : name.limits.out.max * 1) : false;
					});
				}
			})
		}

		/*
		** GET VALUE FOR EXCHANGE OBJECT ON INPUT CHANGE
		** Получение значения поля вывода-ввода
		*/

		$scope.getExchangeValue = function(name) {
			var data = {};
			data.incoming_payed_amount = $scope.amount_value_model;
			data.incoming_currency = $scope.exchange.incoming_currency;
			data.incoming_payment_system  = $scope.exchange.incoming_payment_system;

			data.outgoing_currency = $scope.exchange.outgoing_currency;
			data.outgoing_payment_system = $scope.exchange.outgoing_payment_system;
			data.outgoing_received_amount  = 0;

			if (name != $scope.names[0]) {
				data.incoming_currency = $scope.exchange.outgoing_currency;
				data.incoming_payment_system = $scope.exchange.outgoing_payment_system;
				data.incoming_payed_amount  = $scope.sum_value_model;

				data.outgoing_received_amount = 0;
				data.outgoing_currency = $scope.exchange.incoming_currency;
				data.outgoing_payment_system  = $scope.exchange.incoming_payment_system;
			}
			return data;
		}

		/*
		** get_quotes
		** получение значений из метода get_quotes
		*/

		$scope.get_quotes = function(obj, name) {
			request.send('/exchange/get_quotes', obj, function(response, status) {
				if(status == 200) {
					$scope.success = true;
				}

				if (name == $scope.names[0]) {
					$scope.sum_value_model = response.outgoing_received_amount * 1;
				} else {
					$scope.amount_value_model = response.outgoing_received_amount * 1;
				}
			}, 'post', true);
		}

		/*
		** GET EXCHANGE
		** получение значения из метода при любом изменении
		*/

		$scope.get_exchange = function(model, name) {
			$scope.success = false;
			name = name || $scope.direction;
			var data = $scope.getExchangeValue(name);

			$scope.exchangeRevese = {
				incoming_currency : data.incoming_currency,
				incoming_payed_amount : data.incoming_payed_amount,
				incoming_payment_system : data.incoming_payment_system,

				outgoing_currency : data.outgoing_currency,
				outgoing_payment_system : data.outgoing_payment_system,
				outgoing_received_amount : data.outgoing_received_amount
			}

			if ($scope.exchangeRevese.incoming_payment_system == $scope.exchangeRevese.outgoing_payment_system) {
				logger.logError($scope.text.errors[0]);
			} else {
				if (data.incoming_payed_amount >= $scope.minIn || data.outgoing_received_amount >= $scope.minOut) {
						$scope.get_quotes($scope.exchangeRevese, name);
				}
			}
		}

		/*
		** VALIDATION FIELDS
		** Валидация полей
		*/

		$scope.validation = function(model, value) {
			if (model == 'amount_value_model'){
				value < $scope.minIn ? logger.logError($scope.text.errors[3] + ' ' + $scope.minIn) : true;
				if($scope.maxIn != 0) {
					value > $scope.maxIn ? logger.logError($scope.text.errors[2] + ' ' + $scope.maxIn) : true;
				}
			} else {
				value < $scope.minOut ? logger.logError($scope.text.errors[3] + ' ' + $scope.minOut) : true;
				if($scope.maxOut != 0) {
					value > $scope.maxOut ? logger.logError($scope.text.errors[2] + ' ' + $scope.maxOut) : true;
				}
			}
		}

		/*
		** Кнопка туда-сюда
		*/

		$scope.getDataForChangeDirection = function(data) {
			var data = {};

			$scope.payment_systems.filter(function(item, i){
				if (item.name == $scope.exchange.incoming_payment_system) {
					data.incoming_payment_system = item.name;
					data.incoming_payed_amount = $scope.amount_value_model;
					item.currencies.filter(function(k){
						if (k.code == $scope.exchange.incoming_currency) {
							data.incoming_currency = $scope.exchange.incoming_currency;
							data.minIn = k.limits.in.min * 1;
							data.maxIn = k.limits.in.max * 1;
						}
					});
				}

				if (item.name == $scope.exchange.outgoing_payment_system) {
					data.outgoing_payment_system = item.name;
					data.outgoing_payed_amount  = $scope.sum_value_model;
					item.currencies.filter(function(k){
						if (k.code == $scope.exchange.outgoing_currency) {
							data.outgoing_currency = $scope.exchange.outgoing_currency;
							data.minOut = k.limits.out.min * 1;
							data.maxOut = k.limits.out.max * 1;
						}
					});
				}
				return data;
			});
			return data;
		}

		$scope.changeDirection = function() {
			$scope.changeDir = !$scope.changeDir;
			var data = $scope.getDataForChangeDirection(data);

			var defaultedCurrencyOut = $scope.defaultedCurrencyOut;
			var defaultedCurrencyIn = $scope.defaultedCurrencyIn;

			$scope.minIn = data.minOut;
			$scope.minOut = data.minIn;

			$scope.maxIn = data.maxOut;
			$scope.maxOut = data.maxIn;

			$scope.defaultedCurrencyIn =  defaultedCurrencyOut;
			$scope.defaultedCurrencyOut =  defaultedCurrencyIn;

			$scope.exchange.incoming_currency = data.outgoing_currency;
			$scope.exchange.incoming_payment_system = data.outgoing_payment_system;
			$scope.exchange.incoming_payed_amount = $scope.sum_value_model;
			$scope.amount_value_model = $scope.sum_value_model;

			$scope.exchange.outgoing_currency = data.incoming_currency;
			$scope.exchange.outgoing_payment_system = data.incoming_payment_system;
			$scope.exchange.outgoing_payed_amount = 0;
		}


		/*
		** Переход на следующую страницу ошибка
		*/

		$scope.nextPageError = function() {
			logger.logError($scope.text.errors[1]);
		}

		/*
		** /exchange/transactions
		** Создание транзакции
		*/

		$scope.exchangeTransactions = function() {
			request.send('/exchange/transactions', $scope.exchangeRevese , function(response, status) {
				$scope.transactionsId = response.transaction_id;
				$scope.urlTransactions = '/claim/' + $scope.transactionsId;
				$location.path($scope.urlTransactions);
			}, 'post', true);
		}


		/*
		** FROM THE BACKEND WILL BE VALUE
		*/

		$scope.commission = [6, 5, 2, 1];
	}
})();
