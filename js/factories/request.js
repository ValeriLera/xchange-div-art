(function () {
    'use strict';

    angular.module('app').factory('request', ['$http', '$rootScope', 'logger', request]);

    function request($http, $rootScope, logger) {
        var getToken = function() {
            return localStorage.getItem('_token');
        }

         var getUpdateToken = function() {
            return localStorage.getItem('_tokenUpdate');
        }

      var api_url = 'http://api.stage.xchange.zone'

    	return {
    		send: function(url, post_mas, callback, method, jwt_required) {
                var headers = {};
                jwt_required = jwt_required || false;

    			callback = callback || false;
    			method = method || 'post';

                if(jwt_required) {
                    var _token = getToken();
                    if (_token) {
                      headers['Authorization'] = 'JWT ' + _token;
                    }
                }

    			$http({method : method, url : api_url + url, data : post_mas, headers : headers})
                .then(function success(response) {
                    var data = response.data;
                    var status = response.status;

    				if (callback) {
                        (callback)(data, status);
                    }
    			}, function errorCallback(response) {
                    var data = response.data;
                    var status = response.status;

                    if(status == 402) {
                        var update = getUpdateToken();
                        headers['Authorization'] = 'JWT ' + update;

                        $http({method : 'post', url : api_url + '/auth/access_token' , data : {}, headers : headers})
                        .then(function(response) {
                            localStorage.setItem('_token', response.data.access_token);
                            localStorage.setItem('_tokenUpdate', response.data.refresh_token);
                            success();
                        })
                        return;
                    }

                    logger.logError(response.data.msg);

                    if (callback) {
                        (callback)(data, status);
                    }
                });
    		}
    	};
    };
})();

;
