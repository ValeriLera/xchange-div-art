#!/bin/sh

REMOTE_HOST=xchange-stage

#docker base configs
VERSION=`node -pe "require('./package.json').version"`
BRANCH=`git rev-parse --abbrev-ref HEAD`
BUILD_TIMESTAMP=`date +%s`
REMOTE_DIR=/opt/xchange/versions
SERVICE_DIR=/opt/xchange/vuejs
LATEST_TAG=${BRANCH}/${VERSION}/${BUILD_TIMESTAMP}


echo "Build and push"
npm run build

ssh ${REMOTE_HOST} << EOF
mkdir -p ${REMOTE_DIR}/${LATEST_TAG} || true
exit
EOF

scp -r ./dist/ ${REMOTE_HOST}:${REMOTE_DIR}/${LATEST_TAG}/

ssh ${REMOTE_HOST} << EOF
rm ${SERVICE_DIR} || true
ln -s ${REMOTE_DIR}/${LATEST_TAG}/dist ${SERVICE_DIR}
exit
EOF
