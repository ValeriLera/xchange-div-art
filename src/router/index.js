import Vue from 'vue'
import Router from 'vue-router'

import Main from '@/containers/Main'
import Cabinet from '@/containers/Cabinet'
import Auth from '@/containers/Auth'
import Admin from '@/containers/Admin'
import Exchange from '@/containers/Exchange'
import Action from '@/containers/Action'
import ErrorPage from '@/containers/Error'

import CabinetDashboard from '@/containers/cabinet/CabinetDashboard'
import CabinetExchanges from '@/containers/cabinet/CabinetExchanges'
import CabinetSend from '@/containers/cabinet/CabinetSend'
import CabinetSettings from '@/containers/cabinet/CabinetSettings'

import AuthSignin from '@/containers/auth/AuthSignin'
import AuthSignup from '@/containers/auth/AuthSignup'

import AdminExchanges from '@/containers/admin/AdminExchanges'
import AdminPaymentSystems from '@/containers/admin/AdminPaymentSystems'
import AdminFees from '@/containers/admin/AdminFees'
import AdminCurrencies from '@/containers/admin/AdminCurrencies'
import AdminUsers from '@/containers/admin/AdminUsers'

import ExchangeCreated from '@/containers/exchange/ExchangeCreated'
import ExchangeConfirm from '@/containers/exchange/ExchangeConfirm'

import store from '@/storage'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  linkActiveClass: 'open active',
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {
        selector: to.hash
      }
    }
  },
  routes: [
    {
      path: '/',
      name: 'Main',
      component: Main
    },
    {
      path: '/cabinet',
      name: 'Cabinet',
      redirect: '/cabinet/dashboard',
      component: Cabinet,
      meta: { requiresAuth: true },
      children: [
        {
          path: 'dashboard',
          name: 'CabinetDashboard',
          component: CabinetDashboard,
          meta: { requiresAuth: true }
        },
        {
          path: 'exchanges',
          name: 'CabinetExchanges',
          component: CabinetExchanges,
          meta: { requiresAuth: true }
        },
        {
          path: 'settings',
          name: 'CabinetSettings',
          component: CabinetSettings,
          meta: { requiresAuth: true }
        },
        {
          path: 'send',
          name: 'CabinetSend',
          component: CabinetSend,
          meta: { requiresAuth: true }
        }
      ]
    },
    {
      path: '/auth',
      name: 'Auth',
      redirect: 'auth/signin',
      component: Auth,
      meta: { requiresNoAuth: true },
      children: [
        {
          path: 'signup',
          name: 'AuthSignup',
          component: AuthSignup
        },
        {
          path: 'signin',
          name: 'AuthSignin',
          component: AuthSignin
        }
      ]
    },
    {
      path: '/admin',
      name: 'Admin',
      redirect: '/admin/exchanges',
      component: Admin,
      meta: { requiresAdmin: true },
      children: [
        {
          path: 'exchanges',
          name: 'AdminExchanges',
          component: AdminExchanges
        },
        {
          path: 'users',
          name: 'AdminUsers',
          component: AdminUsers
        },
        {
          path: 'paymentsystems',
          name: 'AdminPaymentSystems',
          component: AdminPaymentSystems
        },
        {
          path: 'fees',
          name: 'AdminFees',
          component: AdminFees
        },
        {
          path: 'currencies',
          name: 'AdminCurrencies',
          component: AdminCurrencies
        }
      ]
    },
    {
      path: '/exchange',
      name: 'Exchange',
      redirect: '/exchange/confirm',
      component: Exchange,
      meta: { exchangeValid: true },
      children: [
        {
          path: 'created',
          name: 'ExchangeCreated',
          component: ExchangeCreated,
          meta: { requiresAuth: true, exchangeConfirmed: true }
        },
        {
          path: 'confirm',
          name: 'ExchangeConfirm',
          component: ExchangeConfirm
        }
      ]
    },
    {
      path: '/confirmation/:token',
      name: 'Action',
      component: Action
    },
    {
      path: '/error',
      name: 'Error',
      component: ErrorPage
    }
  ]
})

export default router

router.beforeEach((to, from, next) => {
  store.dispatch('checkAuth')
  if (store.getters.authData.isAuth) {
    store.dispatch('fetchProfile')
  }
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.getters.authData.isAuth) {
      next({
        path: '/auth'
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.Admin)) {
    if (!store.getters.authData.isAuth) {
      next({
        path: '/auth'
      })
    } else {
      if (!store.getters.userData.profile.is_manager) {
        next('/cabinet')
      } else {
        next()
      }
    }
  } else if (to.matched.some(record => record.meta.requiresNoAuth)) {
    if (store.getters.authData.isAuth) {
      next({
        path: '/cabinet'
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.exchangeValid)) {
    if (!store.getters.exchangeStatus.isValid) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else if (to.matched.some(record => record.meta.exchangeConfirmed)) {
    if (!store.getters.exchangeStatus.isConfirmed) {
      next({
        path: '/'
      })
    } else {
      next()
    }
  } else {
    next()
  }
})
