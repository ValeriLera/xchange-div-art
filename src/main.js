// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from '@/router'

import 'bootstrap/dist/css/bootstrap.css'
import store from '@/storage'

import Datepicker from 'vuejs-datepicker'
import BootstrapVue from 'bootstrap-vue'

import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'

Vue.component('datepicker', Datepicker)
Vue.use(BootstrapVue)

if (process.env.NODE_ENV === 'production') {
  Raven.config('https://0aa32bbc3f0d4336a43d1c89c8f25892@sentry.io/276231')
    .addPlugin(RavenVue, Vue)
    .install()
}

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
