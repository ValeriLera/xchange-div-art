import api from '@/utils/api'
import * as types from '@/storage/mutations-types'
import jwt from 'jsonwebtoken'
import router from '@/router'
import moment from 'moment'

const CONSUME_TOKEN = 'users/confirm'
const CURRENCIES = 'currencies'
const CURRENCY_PAIRS = 'currencies/pairs'
const EXCHANGES = 'exchange/transactions'
const FEES = 'accounting/fees'
const GET_QUOTES_URL = 'exchange/get_quotes'
const PAYMENT_SYSTEMS_URL = 'payment_systems'
const SIGNIN_URL = 'auth/signin'
const USER_PROFILE = 'users/profile'
const USERS = 'users'

export const errorHandler = ({commit}, error) => {
  if (error.response.status >= 500) {
    router.push('/error')
  } else {
    let errorMessage = error.response.data
    commit(types.SET_ERROR, { error: errorMessage })
  }
}

// admin block
export const getTransactions = ({commit}, { limit, offset, status, isManager, toDate, fromDate }) => {
  limit = limit || 25
  offset = offset || null
  status = status || null
  toDate = toDate ? moment(toDate).format('DDMMYY') : null
  fromDate = fromDate ? moment(fromDate).format('DDMMYY') : null
  var collection = isManager ? 'manager' : null
  api.request(EXCHANGES, { authRequred: true, params: { limit, offset, status, collection, to_date: toDate, from_date: fromDate } })
    .then((response) => {
      if (response.status === 200) {
        commit((isManager ? types.SET_ADMIN_TRANSACTIONS : types.GET_USER_TRANSACTIONS), { items: response.data })
      }
    })
    .catch((error) => {
      errorHandler({commit}, error)
    })
}

export const getUsers = ({commit}, { limit, offset }) => {
  limit = limit || 25
  offset = offset || 0
  var isRegistered = true
  api.request(USERS, { authRequred: true, params: { limit, offset, is_registered: isRegistered } })
    .then((response) => {
      if (response.status === 200) {
        commit(types.SET_USERS_LIST, { items: response.data })
      }
    })
    .catch((error) => {
      errorHandler({commit}, error)
    })
}

export const getCurrencyPairs = ({ commit }) => {
  api.request(CURRENCY_PAIRS, { authRequred: true }).then(response => {
    let currencyPairs = response.data
    commit(types.SET_CURRENCY_PAIRS, { items: currencyPairs })
  })
  .catch((error) => {
    errorHandler({commit}, error)
  })
}

export const getCurrencies = ({ commit }) => {
  api.request(CURRENCIES, { authRequred: true }).then(response => {
    let currencies = response.data
    commit(types.SET_CURRENCIES_LIST, { items: currencies })
  })
  .catch((error) => {
    errorHandler({commit}, error)
  })
}

export const getAdminPaymentSystems = ({ commit }) => {
  api.request(PAYMENT_SYSTEMS_URL, {}).then(response => {
    let paymentSystems = response.data
    commit(types.SET_PS_LIST, { items: paymentSystems })
  })
  .catch((error) => {
    errorHandler({commit}, error)
  })
}

export const getFees = ({ commit }) => {
  api.request(FEES, {}).then(response => {
    let fees = response.data
    commit(types.SET_FEES_LIST, { items: fees })
  })
  .catch((error) => {
    errorHandler({commit}, error)
  })
}

// landing
export const getPaymentSystems = ({ commit }) => {
  api.request(PAYMENT_SYSTEMS_URL, {}).then(response => {
    let paymentSystems = response.data
    commit(types.INIT_STATE, { items: paymentSystems })
  })
  .catch((error) => {
    errorHandler({commit}, error)
  })
}

export const selectCurrency = ({ commit }, selected) => {
  commit(types.SELECT_CURRENCY, { items: selected.state.paymentSystem.currencies, id: selected.id, direction: selected.state })
  commit(types.SET_AMOUNT_LIMIT, { direction: selected.state })
}

export const selectPaymentSystem = ({ commit }, selected) => {
  commit(types.SELECT_PAYMENT_SYSTEM, { id: selected.id, direction: selected.state })
  commit(types.SET_AMOUNT_LIMIT, { direction: selected.state })
}

export const changeAmount = ({ commit }, selected) => {
  commit(types.UPDATE_AMOUNT, { value: selected.value, direction: selected.direction })
  commit(types.SET_AMOUNT_LIMIT, { direction: selected.direction })
}

export const getQuotes = ({ commit }, exchangeStatus) => {
  var data = {
    incoming_payed_amount: exchangeStatus.active_way === exchangeStatus.incoming.way ? exchangeStatus.incoming.amount * 1 : 0,
    outgoing_received_amount: exchangeStatus.active_way === exchangeStatus.outgoing.way ? exchangeStatus.outgoing.amount * 1 : 0,
    incoming_payment_system: exchangeStatus.incoming.paymentSystem.name,
    outgoing_payment_system: exchangeStatus.outgoing.paymentSystem.name,
    incoming_currency: exchangeStatus.incoming.currency.code,
    outgoing_currency: exchangeStatus.outgoing.currency.code
  }
  api.request(GET_QUOTES_URL, { method: 'post', data: data }).then(response => {
    let quoteData = response.data
    let valueToUpdate = exchangeStatus.active_way === exchangeStatus.incoming.way ? quoteData.outgoing_received_amount : quoteData.incoming_payed_amount
    let directionToUpdate = exchangeStatus.active_way === exchangeStatus.incoming.way ? exchangeStatus.outgoing : exchangeStatus.incoming
    commit(types.UPDATE_AMOUNT, { value: valueToUpdate, direction: directionToUpdate })
    commit(types.SET_AMOUNT_LIMIT, { direction: directionToUpdate })
    commit(types.APPROVE_EXCHANGE, { isValid: (response.status === 200) })
  })
  .catch((error) => {
    let directionToUpdate = exchangeStatus.active_way === exchangeStatus.incoming.way ? exchangeStatus.outgoing : exchangeStatus.incoming
    commit(types.SET_AMOUNT_LIMIT, { direction: directionToUpdate })
    errorHandler({commit}, error)
  })
}

export const reverseDirection = ({ commit }, exchangeStatus) => {
  commit(types.REVERSE_DIRECTIONS)
  commit(types.SET_AMOUNT_LIMIT, { direction: exchangeStatus.incoming })
  commit(types.SET_AMOUNT_LIMIT, { direction: exchangeStatus.outgoing })
}

export const getIncomingDetails = ({commit}, id) => {
  let incomingDetailsUrl = EXCHANGES + '/' + id + '/get_incoming_details'
  api.request(incomingDetailsUrl, { method: 'post', authRequred: true })
    .then((response) => {
      if (response.status === 200) {
        commit(types.SET_INCOMING_PAYMENT_DETALS, { payload: response.data })
      }
    })
    .catch((error) => {
      errorHandler({commit}, error)
    })
}

export const createTransaction = ({commit}, exchangeStatus) => {
  let data = {
    incoming_payed_amount: exchangeStatus.incoming.amount * 1,
    outgoing_received_amount: 0,
    incoming_payment_system: exchangeStatus.incoming.paymentSystem.name,
    outgoing_payment_system: exchangeStatus.outgoing.paymentSystem.name,
    incoming_currency: exchangeStatus.incoming.currency.code,
    outgoing_currency: exchangeStatus.outgoing.currency.code
  }
  api.request(EXCHANGES, { method: 'post', data: data, authRequred: true })
    .then((response) => {
      if (response.status === 201) {
        commit(types.SET_TRANSACTION_DETALS, { payload: response.data })
        getIncomingDetails({commit}, response.data.transaction_id)
      }
    })
    .catch((error) => {
      errorHandler({commit}, error)
    })
}

export const confirmTransaction = ({commit}, exchangeStatus) => {
  let outgoingDetailsUrl = EXCHANGES + '/' + exchangeStatus.id + '/send_outgoing_details'
  let data = {
    details: exchangeStatus.outgoing.details.data
  }
  api.request(outgoingDetailsUrl, { method: 'post', data: data, authRequred: true })
    .then((response) => {
      if (response.status === 204) {
        commit(types.CONFIRM_EXCHANGE)
        router.push('/exchange/created')
      }
    })
    .catch((error) => {
      errorHandler({commit}, error)
    })
}

export const setOutgoingDetails = ({commit}, details) => {
  commit(types.SET_OUTGOING_PAYMENT_DETALS, { details })
}

// profile data fetch
export const fetchProfile = ({commit}) => {
  api.request(USER_PROFILE, { authRequred: true })
    .then((response) => {
      commit(types.SET_USER_DATA, { payload: response.data })
    })
    .catch((error) => {
      console.log(error)
      errorHandler({commit}, error)
    })
}

export const consumeToken = ({commit}, token) => {
  let outgoingDetailsUrl = CONSUME_TOKEN + '/' + token
  api.request(outgoingDetailsUrl, {})
    .then((response) => {
      if (response.status === 200) {
        router.push('/cabinet')
      } else {
        router.push('/error')
      }
    })
    .catch((error) => {
      errorHandler({commit}, error)
    })
}

export const createProfile = ({commit}, userDetails) => {
  api.request(USER_PROFILE, { method: 'put', authRequred: true, data: userDetails })
    .then((response) => {
      if (response.code === 204) {
        router.push('/cabinet')
      }
    })
    .catch((error) => {
      errorHandler({commit}, error)
    })
}

export const updateProfile = ({commit}, userDetails) => {
  for (var k in userDetails) {
    if (!userDetails[k]) {
      delete userDetails[k]
    }
  }
  api.request(USER_PROFILE, { method: 'put', authRequred: true, data: userDetails })
    .then((response) => {
      if (response.code === 204) {
        commit(types.UPDATE_USER_DATA, { payload: userDetails })
      }
    })
    .catch((error) => {
      errorHandler({commit}, error)
    })
}

// Auth block
const SIGNUP_URL = 'auth/signup'
const REFRESH_URL = 'auth/access_token'

const ACCESS_TOKEN_NAME = '_accessToken'
const UPDATE_TOKEN_NAME = '_updateToken'

const PUBLIC_KEY = process.env['PUBLIC_KEY'] || '-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCeaynl1HsyTo/f8Ot3C7l4VSFa\nfJu55r63xGK5iJ03FEEs1HXw2rNAk9DqR9366srU8RPskrMffDJek1J9F++FzMNS\ngScK5lb8wFJ/nuZpYvtPV8uJjS3RjpPpX3PhjqF2/YaP0rOeBpCprZW14cYru7H0\nDvjmPKTWVEN/aRRTQQIDAQAB\n-----END PUBLIC KEY-----'
const ALGORITHM = 'RS256'

export const setAuth = ({ commit }, isAuth) => {
  commit(types.SET_AUTH, { isAuth })
}

export const signIn = ({ commit }, credentials) => {
  if (credentials.email && credentials.password) {
    api.request(SIGNIN_URL, { method: 'post', data: credentials })
      .then((response) => {
        if (response.status === 200) {
          setAccessToken(response.data)
          setUpdateToken(response.data)
          commit(types.SET_AUTH, { isAuth: true })
          decodeToken({ commit })
          router.push('/cabinet')
        }
      })
      .catch((error) => {
        errorHandler({commit}, error)
      })
  }
}

export const signUp = ({ commit }) => {
  return api.request(SIGNUP_URL, { method: 'post' })
    .then((response) => {
      if (response.status === 201) {
        setAccessToken(response.data)
        setUpdateToken(response.data)
        commit(types.SET_AUTH, { isAuth: true })
        decodeToken({ commit })
      }
    })
    .catch((error) => {
      errorHandler({commit}, error)
    })
}

export const signOut = ({ commit }) => {
  dropAuthTokens()
  commit(types.SET_AUTH, { isAuth: false })
}

export const decodeToken = ({ commit }) => {
  try {
    var tokenPayload = jwt.decode(
      localStorage.getItem(ACCESS_TOKEN_NAME),
      PUBLIC_KEY,
      {algorithm: ALGORITHM}
    )
    commit(types.SET_USER_TOKEN_DATA, { payload: tokenPayload })
  } catch (err) {
  }
}

export const checkAuth = ({ commit }) => {
  if (localStorage.getItem(ACCESS_TOKEN_NAME) || localStorage.getItem(UPDATE_TOKEN_NAME)) {
    try {
      jwt.verify(
        localStorage.getItem(ACCESS_TOKEN_NAME),
        PUBLIC_KEY,
        {algorithm: ALGORITHM, ignoreNotBefore: true}
      )
      commit(types.SET_AUTH, { isAuth: true })
      decodeToken({ commit })
    } catch (err) {
      localStorage.removeItem(ACCESS_TOKEN_NAME)
      let headers = getUpdateHeader()
      api.request(REFRESH_URL, { method: 'post', headers: headers })
        .then((response) => {
          if (response.status === 200) {
            localStorage.setItem(ACCESS_TOKEN_NAME, response.data.access_token)
            commit(types.SET_AUTH, { isAuth: true })
            decodeToken({ commit })
          }
        })
        .catch((err) => {
          if (err.response.status >= 500) {
            router.push('/error')
          }
          if (err.response.status === 422) {
            console.log('Refresh token invalid!', err.response.data)
            localStorage.removeItem(UPDATE_TOKEN_NAME)
          }
        })
    }
  }
}

export const dropAuthTokens = () => {
  localStorage.removeItem(ACCESS_TOKEN_NAME)
  localStorage.removeItem(UPDATE_TOKEN_NAME)
}

export const getAuthHeader = () => {
  return {'Authorization': 'JWT ' + localStorage.getItem(ACCESS_TOKEN_NAME)}
}

export const getUpdateHeader = () => {
  return {'Authorization': 'JWT ' + localStorage.getItem(UPDATE_TOKEN_NAME)}
}

export const setAccessToken = (data) => {
  localStorage.setItem(ACCESS_TOKEN_NAME, data.access_token)
}

export const setUpdateToken = (data) => {
  localStorage.setItem(UPDATE_TOKEN_NAME, data.refresh_token)
}
