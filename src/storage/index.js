import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

import * as actions from '@/storage/actions'
import items from '@/storage/items'

Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const store = new Vuex.Store({
  actions,
  modules: {
    items
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})

export default store
