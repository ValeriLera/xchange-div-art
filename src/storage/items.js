import * as types from '@/storage/mutations-types'

// initial state
const state = {
  paymentSystems: [],
  exchangeStatus: {
    id: null,
    isValid: false,
    isConfirmed: false,
    active_way: 'in',
    incoming: {
      way: 'in',
      currency: {},
      amount: 0,
      paymentSystem: {},
      details: {
        data: '',
        description: ''
      }
    },
    outgoing: {
      way: 'out',
      currency: {},
      amount: 0,
      paymentSystem: {},
      details: {
        data: '',
        description: ''
      }
    }
  },
  userData: {
    profile: {},
    transactions: []
  },
  authData: {
    isAuth: false,
    tokenPayload: {}
  },
  errors: null,
  systemState: {
    currencyPairs: [],
    transactions: [],
    users: [],
    currencies: [],
    fees: [],
    paymentSystems: []
  }
}

// getters
const getters = {
  paymentSystems: state => state.paymentSystems,
  exchangeStatus: state => state.exchangeStatus,
  userData: state => state.userData,
  authData: state => state.authData,
  errors: state => state.errors,
  systemState: state => state.systemState
}

const mutations = {
  [types.INIT_STATE] (state, { items }) {
    state.errors = null
    state.exchangeStatus = {
      id: null,
      isValid: false,
      isConfirmed: false,
      active_way: 'in',
      incoming: { way: 'in', currency: {}, amount: 0, paymentSystem: {}, details: { data: '', description: '' }
      },
      outgoing: { way: 'out', currency: {}, amount: 0, paymentSystem: {}, details: { data: '', description: '' }
      }
    }
    state.paymentSystems = items.filter((obj) => { return obj.currencies.length > 0 })
    var defaultInPs = state.paymentSystems.filter((obj) => { return obj.default_in && obj.is_active_in })[0]
    var defaultOutPs = state.paymentSystems.filter((obj) => { return obj.default_out && obj.is_active_out })[0]
    state.exchangeStatus.incoming.paymentSystem = defaultInPs
    state.exchangeStatus.outgoing.paymentSystem = defaultOutPs
    var defaultInCur = defaultInPs.currencies.filter((obj) => { return obj.is_active_in })[0]
    var defaultOutCur = defaultOutPs.currencies.filter((obj) => { return obj.is_active_out })[0]
    state.exchangeStatus.incoming.currency = defaultInCur
    state.exchangeStatus.outgoing.currency = defaultOutCur
    var defaultInAmount = defaultInCur.limits.in.min
    var defaultOutAmount = defaultOutCur.limits.out.min
    state.exchangeStatus.incoming.amount = defaultInAmount
    state.exchangeStatus.outgoing.amount = defaultOutAmount
  },
  [types.SELECT_CURRENCY] (state, { items, id, direction }) {
    state.errors = null
    state.exchangeStatus.active_way = direction.way
    var newCurrency = items.filter((obj) => { return obj.id === id })[0]
    direction.currency = newCurrency
  },
  [types.SELECT_PAYMENT_SYSTEM] (state, { id, direction }) {
    state.errors = null
    state.exchangeStatus.active_way = direction.way
    var newPaymentSystem = state.paymentSystems.filter((obj) => { return obj.id === id })[0]
    direction.paymentSystem = newPaymentSystem
    var newCurrency = newPaymentSystem.currencies.filter((obj) => { return obj.code === direction.currency.code })[0]
    if (!newCurrency) {
      newCurrency = newPaymentSystem.currencies.filter((obj) => { return obj })[0]
    }
    direction.currency = newCurrency
  },
  [types.SET_AMOUNT_LIMIT] (state, { direction }) {
    state.errors = null
    if (direction.amount < direction.currency.limits[direction.way].min) {
      direction.amount = direction.currency.limits[direction.way].min
    }
    if ((direction.amount > direction.currency.limits[direction.way].max) && (direction.currency.limits[direction.way].max * 1 !== 0)) {
      direction.amount = direction.currency.limits[direction.way].max
    }
  },
  [types.UPDATE_AMOUNT] (state, { value, direction }) {
    state.errors = null
    state.exchangeStatus.active_way = direction.way
    direction.amount = value
  },
  [types.APPROVE_EXCHANGE] (state, { isValid }) {
    state.errors = null
    state.exchangeStatus.isValid = isValid
  },
  [types.CONFIRM_EXCHANGE] (state) {
    state.errors = null
    state.exchangeStatus.isConfirmed = true
  },
  [types.REVERSE_DIRECTIONS] (state) {
    state.errors = null
    var incomingDirection = state.exchangeStatus.incoming
    var outgoingDirection = state.exchangeStatus.outgoing
    incomingDirection.way = 'out'
    outgoingDirection.way = 'in'
    state.exchangeStatus.incoming = outgoingDirection
    state.exchangeStatus.outgoing = incomingDirection
    state.exchangeStatus.active_way = outgoingDirection.way
  },
  [types.SET_AUTH] (state, { isAuth }) {
    state.errors = null
    state.authData.isAuth = isAuth
    if (!isAuth) {
      state.authData.tokenPayload = {}
    }
  },
  [types.SET_USER_TOKEN_DATA] (state, { payload }) {
    state.errors = null
    state.authData.tokenPayload = payload
  },
  [types.UPDATE_USER_DATA] (state, { payload }) {
    state.errors = null
    state.authData.profile.email = payload.email
    state.authData.profile.name = payload.name
    state.authData.profile.phone = payload.phone
  },
  [types.SET_USER_DATA] (state, { payload }) {
    state.errors = null
    state.userData.profile = payload
  },
  [types.SET_TRANSACTION_DETALS] (state, { payload }) {
    state.errors = null
    state.exchangeStatus.id = payload.transaction_id
    state.exchangeStatus.outgoing.amount = payload.outgoing_received_amount
  },
  [types.SET_INCOMING_PAYMENT_DETALS] (state, { payload }) {
    state.errors = null
    state.exchangeStatus.incoming.amount = payload.bill_amount
    state.exchangeStatus.incoming.details.data = payload.details
    state.exchangeStatus.incoming.details.description = payload.incoming_details_description
    state.exchangeStatus.outgoing.details.description = payload.outgoing_details_description
  },
  [types.SET_OUTGOING_PAYMENT_DETALS] (state, { details }) {
    state.errors = null
    state.exchangeStatus.outgoing.details.data = details.details
  },
  // cabinet mutations
  [types.GET_USER_TRANSACTIONS] (state, { items }) {
    state.errors = null
    state.userData.transactions = items
  },
  [types.SET_ERROR] (state, { error }) {
    state.errors = error
  },
  // admin mutations
  [types.SET_CURRENCY_PAIRS] (state, { items }) {
    state.errors = null
    state.systemState.currencyPairs = items
  },
  [types.SET_ADMIN_TRANSACTIONS] (state, { items }) {
    state.errors = null
    state.systemState.transactions = items
  },
  [types.SET_USERS_LIST] (state, { items }) {
    state.errors = null
    state.systemState.users = items
  },
  [types.SET_CURRENCIES_LIST] (state, { items }) {
    state.errors = null
    state.systemState.currencies = items
  },
  [types.SET_PS_LIST] (state, { items }) {
    state.errors = null
    state.systemState.paymentSystems = items
  },
  [types.SET_FEES_LIST] (state, { items }) {
    state.errors = null
    state.systemState.fees = items
  }
}

export default {
  state,
  getters,
  mutations
}
