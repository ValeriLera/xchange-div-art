import axios from 'axios'
import store from '@/storage'
import { getAuthHeader } from '@/storage/actions'

const API_SCHEMA = process.env['API_SCHEMA'] || 'http'
const API_URL = process.env['API_HOST']
const API_PORT = [80, 443].indexOf(process.env['API_PORT']) === -1 ? ':' + process.env['API_PORT'] : ''
const TIMEOUT = process.env['TIMEOUT'] || 2000

const BASE_URL = API_SCHEMA + '://' + API_URL + API_PORT

axios.defaults.headers.common['Content-Type'] = 'application/json'
axios.defaults.baseURL = BASE_URL
axios.defaults.timeout = TIMEOUT
axios.defaults.responseType = 'json'
axios.defaults.validateStatus = (status) => { return status >= 200 && status <= 301 }

export default {
  request (url, { method, headers, params, data, authRequred }) {
    method = method || 'get'
    headers = headers || {}
    data = data || {}
    authRequred = authRequred || false
    params = params || {}
    if (authRequred || store.getters.authData.isAuth) {
      headers = getAuthHeader()
    }
    return axios({
      method: method,
      url: url,
      data: data,
      params: params,
      headers: headers
    })
  }
}
