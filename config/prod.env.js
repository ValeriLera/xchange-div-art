'use strict'
module.exports = {
  NODE_ENV: '"production"',

  // backend api params
  API_HOST: '"api.stage.xchange.zone"',
  API_PORT: '80'
}
